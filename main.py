# coding=utf-8
import random
import time
from itertools import permutations


def find_objects_for(objects, search_filter):
    matching_objects = []
    for object_with_tags in objects:
        tags = object_with_tags.get('tags', set())
        for tag in tags:
            if search_filter(tag):
                matching_objects.append(object_with_tags)
                break
    return matching_objects


def find_objects_generator(objects, search_filter):
    for object_with_tags in objects:
        tags = object_with_tags.get('tags', set())
        for tag in tags:
            if search_filter(tag):
                yield object_with_tags
                break


NUMBER_OF_OBJECTS = 100000
NUMBER_OF_TAGS = 20
random.seed(0)
possible_tags = ["".join(x) for x in permutations("abcdefg")]
objects = [{'id': x, 'tags': random.choices(possible_tags, k=NUMBER_OF_TAGS)} for x in range(NUMBER_OF_OBJECTS)]


if __name__ == '__main__':
    t = time.time()
    matches = find_objects_for(objects, lambda x: x.startswith('abc'))
    print(f"found {len(matches)} matches in {(time.time()-t)*1000:.2f}ms using for")
    t = time.time()
    matches = find_objects_generator(objects, lambda x: x.startswith('abc'))
    print(f"generator ready in {(time.time() - t)*1000:.2f}ms")
    t = time.time()
    matches = list(matches)
    print(f"realized {len(matches)} in matches in {(time.time() - t)*1000:.2f}ms")
