from unittest import TestCase

from main import find_objects_for, find_objects_generator

test_objects = [
    {'name': 'object1', 'tags': ['tag1'], },
    {'name': 'object2', 'tags': [], },
    {'name': 'object3', 'tags': ['othertag', 'tag1'], },
    {'name': 'object4', 'tags': ['tag1', 'tag2'], },
]


class Test(TestCase):

    def test_find_objects_for(self):
        self.assertEqual([test_objects[0], test_objects[2], test_objects[3]],
                         find_objects_for(test_objects, lambda x: x.startswith('t')))

    def test_find_objects_generator(self):
        self.assertEqual([test_objects[0], test_objects[2], test_objects[3]],
                         list(find_objects_generator(test_objects, lambda x: x.startswith('t'))))
